# specify a base image
FROM node:alpine

WORKDIR /usr/VijayBhaskar/Documents/DockerList/simpleweb


COPY ./package.json ./
RUN npm install 

COPY ./ ./

CMD ["npm", "start"]

